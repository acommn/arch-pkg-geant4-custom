# Maintainer: acommn <acommn@gmail.com>
pkgname=geant4-custom
pkgver=10.05
pkgrel=1
epoch=
pkgdesc="Geant4 is a toolkit for the simulation of the passage of particles through matter."
arch=('x86_64')
url="http://geant4.web.cern.ch/"
license=('custom')
groups=()
depends=('cmake>=3.3.0' 'freetype2' 'gcc' 'libx11' 'mesa' 'openmotif' 'qt5-base' 'zlib>=1.2.3')
makedepends=()
checkdepends=()
optdepends=()
provides=()
conflicts=('geant4')
replaces=()
backup=()
options=(!strip)
install=$pkgname.install
changelog=
source=(http://${pkgname%-custom}-data.web.cern.ch/${pkgname%-custom}-data/releases/${pkgname%-custom}.$pkgver.tar.gz)
noextract=()
sha256sums=('4b05b4f7d50945459f8dbe287333b9efb772bd23d50920630d5454ec570b782d')

prepare() {
  cd "$srcdir";
  # Create temporal build directory.
  if [[ ! -d "${pkgname%-custom}-build" ]]; then
    mkdir "${pkgname%-custom}-build";
  fi;
}

build() {
  cd "$srcdir/${pkgname%-custom}-build";
  cmake -DCMAKE_INSTALL_PREFIX="/usr"\
        -DCMAKE_INSTALL_LIBDIR="lib"\
        -DCMAKE_BUILD_TYPE="Release"\
        -DGEANT4_BUILD_MULTITHREADED="ON"\
        -DGEANT4_INSTALL_DATA="ON"\
        -DGEANT4_USE_OPENGL_X11="ON"\
        -DOpenGL_GL_PREFERENCE="GLVND"\
        -DGEANT4_USE_RAYTRACER_X11="ON"\
        -DGEANT4_USE_QT="ON"\
        -DGEANT4_USE_XM="ON"\
        -DGEANT4_USE_SYSTEM_ZLIB="ON"\
        -DGEANT4_INSTALL_EXAMPLES="ON"\
        -DGEANT4_USE_FREETYPE="ON"\
        "../${pkgname%-custom}.$pkgver";
  make --jobs=$(nproc) --output-sync=target --no-print-directory --silent;
}

package() {
  cd "$srcdir/${pkgname%-custom}-build";
  make --no-print-directory --silent DESTDIR="$pkgdir" install;
  install --owner=root --group=root --mode=0644 -D "../${pkgname%-custom}.$pkgver/LICENSE" "$pkgdir/usr/share/licenses/${pkgname%-custom}/LICENSE";
}

# vim:set ts=2 sw=2 et:
